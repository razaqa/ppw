from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Razaqa Dhafin Haffiyan'
univ = 'Universitas Indonesia'
hobby = 'Make Something'
desc = 'I am a big dreamer'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,4,5)
npm = 1706039484
img_url = 'https://bem.cs.ui.ac.id/staf/assets/2018/img/photo/1706039484.jpg'
bckgrd_url = 'https://images.adsttc.com/media/images/5017/aeef/28ba/0d44/3100/0c6d/slideshow/stringio.jpg?1414573411'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year),
                'birth':birth_date, 'univ':univ, 'npm': npm, 'desc':desc,
				'hobby':hobby, 'img_url':img_url, 'bckgrd_url':bckgrd_url}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
